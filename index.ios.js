/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  WebView,
  TouchableHighlight,
  Alert
} from 'react-native';
import PushNotification from 'react-native-push-notification';

export default class prob extends Component {
  constructor(props) {
    super(props);
    this.webView = null;
  }

  testNoti() {
    PushNotification.configure({
      onNotification: function(notification) {
        console.log('NOTIFICATION:', notification);
      }
    });
  }

  onMessage(event) {
    switch(event.nativeEvent.data) {
      case "alert":
        Alert.alert(
         'LOOK AT THIS',
         'OMG this is an alert!',
         [
            {text: 'Maybe'},
            {text: 'No', style: 'cancel'},
            {text: 'Yes'},
         ]
      )
        break;
      case "notification":
        this.testNoti;
        break;
      default:
        break;
    }
  }


  sendPostMessage(message) {
    console.log(message);
    this.webView.postMessage(message);
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableHighlight style={{padding: 10, backgroundColor: 'blue', marginTop: 20}} onPress={() => this.sendPostMessage("clear")}>
          <Text style={{color: 'white'}}>Clear Picture</Text>
        </TouchableHighlight>

        <TouchableHighlight style={{padding: 10, backgroundColor: 'red', marginTop: 20}} onPress={() => this.sendPostMessage("meme")}>
          <Text style={{color: 'white'}}>Add a meme</Text>
        </TouchableHighlight>

        <WebView
          javaScriptEnabled={true}  
          domStorageEnabled={true}  
          scalesPageToFit={false}
          source={{uri: 'https://stormy-reef-10897.herokuapp.com/'}}
          ref={( webView ) => this.webView = webView}
          onMessage={this.onMessage} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f2f2',
    paddingTop:20,
  },
});

AppRegistry.registerComponent('firstTest', () => prob);